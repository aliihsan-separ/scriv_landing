@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">
            @include('layouts.alerts')
            <div class="row">

                <div class="col-md-6">
                    <h1>People Trust Us: </h1>
                    <div class="live">
                        @foreach($activities as $activity)
                            <div class="info-popup animated fadeInLeft slow" onclick="swapDiv(event,this);">
                                <small class="time">{!! $activity->created_at->format('H:i:s') !!}</small>
                                <div class="info">
                                    {!! $activity->description !!} {{--<span style="font-size: 40px;"> 311 SCRIV</span>--}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="section1">
                        <div class="box"><i class="fa fa-5x fa-money" aria-hidden="true"></i>
                            <span>No prepayment
                    <span>0% fee deposit and withdrawal  </span></span>
                        </div>
                        <div class="box"><i class="fa fa-5x fa-server" aria-hidden="true"></i>
                            <span>Superior service security
                    <span>Veriﬁed by professionals - <a
                                href="{!! url('/service_security') !!}">Learn More</a> </span></span>
                        </div>
                        <div class="box"><i class="fa fa-5x fa-check-square-o" aria-hidden="true"></i>
                            <span>Instant in everything
                    <span>Deposits, rewards, reinvest, withdrawal got no lock or delay.  </span></span>
                        </div>

                    </div>

                </div>

                <div class="section2">
                    <div class="row">
                        <div class="col-md-8">

                        </div>
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-9">
                            <div class="listed animated fadeInLeft">
                                <h1 class="top-coins-section__title">Listed Coins</h1>
                                <div class="row">

                                    <div class="coin-info text-center">
                                        <a href="javascript:void(0)">
                                            <div class="ribbon ribbon-top-left"><span>New &amp; Hot</span></div>
                                            <img src="images/mfit.svg" alt="">
                                            <div class="mb-2">
                                                <h5 class="font-bold no-margins">
                                                    MFIT COIN
                                                </h5>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="coin-info text-center">
                                        <a href="javascript:void(0)">
                                            <img src="images/apollon.svg" alt="">
                                            <div class="mb-2">
                                                <h5 class="font-bold no-margins">
                                                    Apollon
                                                </h5>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="coin-info text-center">
                                        <a href="javascript:void(0)">
                                            <img src="images/bettex.svg" alt="">
                                            <div class="mb-2">
                                                <h5 class="font-bold no-margins">
                                                    Bettex
                                                </h5>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="coin-info text-center">
                                        <a href="javascript:void(0)">
                                            <div class="ribbon ribbon-top-left"><span>New &amp; Hot</span></div>
                                            <img src="images/mfit.svg" alt="">
                                            <div class="mb-2">
                                                <h5 class="font-bold no-margins">
                                                    MFIT COIN
                                                </h5>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="coin-info text-center">
                                        <a href="javascript:void(0)">
                                            <img src="images/apollon.svg" alt="">
                                            <div class="mb-2">
                                                <h5 class="font-bold no-margins">
                                                    Apollon
                                                </h5>
                                            </div>
                                        </a>
                                    </div>


                                    <div class="coin-info text-center">
                                        <a href="javascript:void(0)">
                                            <img src="images/bettex.svg" alt="">
                                            <div class="mb-2">
                                                <h5 class="font-bold no-margins">
                                                    Bettex
                                                </h5>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <a href="#"><h4 style="float:right;">Suggest to your coin<b
                                                style="padding-left: 5px;">+</b>
                                    </h4></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="partners animated fadeInRight">
                                <h1> Partners </h1>

                                <div class="coin-info text-center" style="margin-bottom: 34px">
                                    <a href="javascript:void(0)">
                                        <img src="images/kyd.svg" alt="">
                                        <div class="mb-2">
                                            <h5 class="font-bold no-margins">
                                                KYD
                                            </h5>
                                        </div>
                                    </a>
                                </div>

                                <a href="#" class="become_a_partner" data-user="{!! auth()->user()->id !!}"><h4
                                            style="float:left;"><b style="padding-right: 5px;">+</b> Become a partner
                                    </h4></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer-primary">
        <div class="container">
            <div class="row text-sm-left">
                <div class="col-md-3 pull-left">
                    <a href="/" class="f-logo">
                        <img class="logo__image" src="https://stackofstake.com/images/logo.svg" alt="">
                        <span class="logo__text">StackOfStake (beta)</span>
                    </a>
                </div>
                <div class="col-md-2">
                    <ul class="footer-social x-center">
                        <li>
                            <a target="_blank" href="mailto:contact@stackofstake.com"><i class="fa fa-envelope"
                                                                                         aria-hidden="true"></i></a>
                        </li>
                        <li>
                        </li>
                        <li>
                            <a target="_blank" href="https://bitcointalk.org/index.php?topic=3295436"><i
                                    class="fab fa-bitcoin" aria-hidden="true"></i></a>
                        </li>
                        <li>
                        </li>
                        <li>
                            <a target="_blank" href="https://discord.gg/tYcb8uK"><i class="fab fa-discord"
                                                                                    aria-hidden="true"></i></a>
                        </li>
                        <li>
                        </li>
                        <li>
                            <a target="_blank" href="https://twitter.com/StackOfStake"><i class="fab fa-twitter"
                                                                                          aria-hidden="true"></i></a>
                        </li>
                        <li>
                        </li>
                    </ul>
                </div>
                <div class="col-md-1 ">
                    <ul class="list-unstyled">
                        <li><a href="https://stackofstake.com/Home/Partnership">Listing</a></li>
                        <li><a href="https://stackofstake.com/Home/Careers">Careers</a></li>
                        <li><a href="mailto:contact@stackofstake.com">Email us</a></li>
                    </ul>
                </div>
                <div class="col-md-2 ">
                    <ul class="list-unstyled">
                        <li><a href="https://stackofstake.com/Home/ServiceAgreement">Service Agreement</a></li>
                        <li><a href="https://stackofstake.com/Home/TermsOfUse">Terms Of Use</a></li>
                        <li><a href="https://stackofstake.com/Home/PrivacyPolicy">Privacy Policy</a></li>
                        <li><a href="https://stackofstake.com/Home/CookiePolicy">Cookie Policy</a></li>
                    </ul>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-2"></div>

            </div>
        </div>
        <div class="container">
            <div class="col-sm-12">
                <p class="text-center footer-copyright">
                    © 2018 - <a target="_blank" href="https://scriv.network">Scriv Network</a>, <a href="#">StackOfStake</a>.
                    All Rights Reserved.
                </p>
            </div>
        </div>
        </div>
    </footer>
@endsection
@push('scripts')
    <script>
        var count = 4;
        $('.become_a_partner').on('click', function (e) {
            e.preventDefault();
            var user = $(this).data('user');
            $.ajax({
                url: '/addPartnerLog',
                type: 'post',
                data: {'user': user},
                success: function (data) {

                    if(data !== 'error') {
                        $('.live:first').prepend(
                            '<div class="info-popup animated fadeInLeft slow" onclick="swapDiv(event,this);">' +
                            '  <small class="time">{!! $activity->created_at->format('H:i:s') !!}</small>' +
                            '     <div class="info">' +
                            data +
                            '     </div>' +
                            '</div>'
                        );

                        $('.info-popup:last-child').remove();
                    }else{
                        toastr["error"]("You already be a partner. You can't be again!")
                    }


                }
            });
        });

        function swapDiv(event, elem) {
            elem.parentNode.insertBefore(elem, elem.parentNode.firstChild);
        }
    </script>
@endpush
