<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Title</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="{!! asset('/css/bootstrap.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/css/font-awesome.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/css/style.css?v=3') !!}"/>
</head>

<body class="info">

<header>
    <nav class="navbar navbar-expand-xl navbar-primary fixed-top" role="navigation">
        <div class="container">
            <a class="navbar-brand logo" href="{!! auth()->guest() ? url('/') : route('home') !!}">
                <img src="https://stackofstake.com/images/logo_white.svg" alt="" class="logo__image"><span>StackOfStake</span>

            </a>
            <button type="button" class="navbar-toggler navbar-light" data-toggle="collapse"
                    data-target=".navbar-collapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse pull-right justify-content-end">
                <ul class="nav navbar-nav">
                    @guest
                        <li><a href="{!! url('/') !!}">How It Works</a></li>
                        <li><a href="{!! url('/') !!}">FAQ</a></li>
                        <li><a href="{!! url('/#home-tab') !!}">Login</a></li>
                    @else
                        <li><a href="{!! route('home') !!}">How It Works</a></li>
                        <li><a href="{!! route('home') !!}">FAQ</a></li>
                    @endif
                </ul>

            </div>
        </div>
    </nav>
</header>

<div class="container">
    <div class="info-1">
        <div class="title">Service Security</div>

        <div class="info-content">
        <span>
        <i class="fa fa-shield" aria-hidden="true"></i></span>
            <span>
            <div class="itext">Secure coded frontend and backend</div>
            <div class="itext">We are using only the most secure programming languages and techniques</div>
        </span>
        </div>

        <div class="info-content">
        <span>
        <i class="fa fa-shield" aria-hidden="true"></i></span>
            <span>
            <div class="itext">Servers protection</div>
            <div class="itext">Servers are strongly protected from any unauthorized access</div>
        </span>
        </div>

        <div class="info-content">
        <span>
        <i class="fa fa-shield" aria-hidden="true"></i></span>
            <span>
            <div class="itext">Emergency service shutdown</div>
            <div class="itext">For any suspicion of hacking, service will be automatically shuted down</div>
        </span>
        </div>

        <div class="info-content">
        <span>
        <i class="fa fa-shield" aria-hidden="true"></i></span>
            <span>
            <div class="itext">Service personnel security awareness</div>
            <div class="itext">All staff got a good knowledge of personal and corporate cyber-security</div>
        </span>
        </div>
    </div>

</div>

<div class="title">Latest independent audits:</div>

<div class="info-2">
    <div class="container">
        <div class="row">
            <div class="col-2"><img src="images/auditreport.jpg" alt=""></div>
            <div class="col-10">
                <div class="title-2"><span>Web application security audit</span><span>by Cryptech Services</span></div>
                <div class="date d-inline-block">Date: November 18th, 2018</div>
                <a href="https://drive.google.com/file/d/1Z5ZNRbA3VwcLgnpCn30w-gRGF4oWOv3w/view" target="_blank"> <button type="button" class="btn btn-light float-right btn-lg">View Audit Report</button></a>
            </div>
        </div>
    </div>
</div>

<div class="info-3">
    <div class="container">
        <div class="row">
            <div class="col-2"><img src="images/assessment.jpg" alt=""></div>
            <div class="col-10">
                <div class="title-2"><span>Cyber Security Assessment audit</span><span>by Hacken</span></div>
                <div class="date d-inline-block">Date: Planned</div>
                <button type="button" class="btn btn-light float-right btn-lg">Reporting cooming soon</button>
            </div>
        </div>
    </div>
</div>

<div class="stats">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <div class="box"><span>Time since service launch</span><span>104 days</span><span>Service launch date: 08.10.2018</span>
                </div>
            </div>
            <div class="col-4">
                <div class="box"><span>Time since service launch</span><span>104 days</span><span>Service launch date: 08.10.2018</span>
                </div>
            </div>
            <div class="col-4">
                <div class="box"><span>Time since service launch</span><span>104 days</span><span>Service launch date: 08.10.2018</span>
                </div>
            </div>


        </div>
    </div>
</div>
<footer class="footer-primary">
    <div class="container">
        <div class="row text-sm-left">
            <div class="col-md-3 pull-left">
                <a href="/" class="f-logo">
                    <img class="logo__image" src="https://stackofstake.com/images/logo.svg" alt="">
                    <span class="logo__text">StackOfStake (beta)</span>
                </a>
            </div>
            <div class="col-md-2">
                <ul class="footer-social x-center">
                    <li>
                        <a target="_blank" href="mailto:contact@stackofstake.com"><i class="fa fa-envelope"
                                                                                     aria-hidden="true"></i></a>
                    </li>
                    <li>
                    </li>
                    <li>
                        <a target="_blank" href="https://bitcointalk.org/index.php?topic=3295436"><i
                                class="fab fa-bitcoin" aria-hidden="true"></i></a>
                    </li>
                    <li>
                    </li>
                    <li>
                        <a target="_blank" href="https://discord.gg/tYcb8uK"><i class="fab fa-discord"
                                                                                aria-hidden="true"></i></a>
                    </li>
                    <li>
                    </li>
                    <li>
                        <a target="_blank" href="https://twitter.com/StackOfStake"><i class="fab fa-twitter"
                                                                                      aria-hidden="true"></i></a>
                    </li>
                    <li>
                    </li>
                </ul>
            </div>
            <div class="col-md-1 ">
                <ul class="list-unstyled">
                    <li><a href="https://stackofstake.com/Home/Partnership">Listing</a></li>
                    <li><a href="https://stackofstake.com/Home/Careers">Careers</a></li>
                    <li><a href="mailto:contact@stackofstake.com">Email us</a></li>
                </ul>
            </div>
            <div class="col-md-2 ">
                <ul class="list-unstyled">
                    <li><a href="https://stackofstake.com/Home/ServiceAgreement">Service Agreement</a></li>
                    <li><a href="https://stackofstake.com/Home/TermsOfUse">Terms Of Use</a></li>
                    <li><a href="https://stackofstake.com/Home/PrivacyPolicy">Privacy Policy</a></li>
                    <li><a href="https://stackofstake.com/Home/CookiePolicy">Cookie Policy</a></li>
                </ul>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-2"></div>

        </div>
    </div>
    <div class="container">
        <div class="col-sm-12">
            <p class="text-center footer-copyright">
                © 2018 - <a target="_blank" href="https://scriv.network">Scriv Network</a>, <a href="#">StackOfStake</a>.
                All Rights Reserved.
            </p>
        </div>
    </div>
    </div>
</footer>
<!--Jquery-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js"></script>
<!--Bootstrap-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!-- Scipts -->
<script src="{!! asset('/js/main.js') !!}" type="text/javascript"></script>

</body>
</html>
