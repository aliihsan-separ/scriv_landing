<div class="section2">
    <div class="row">
        <div class="col-md-8">

        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-9">
            <div class="listed">
                <h1 class="top-coins-section__title"> Listed Coins </h1>
                <div class="row">

                    <div class="coin-info text-center">
                        <a href="javascript:void(0)">
                            <div class="ribbon ribbon-top-left"><span>New &amp; Hot</span></div>
                            <img src="images/mfit.svg" alt="">
                            <div class="mb-2">
                                <h5 class="font-bold no-margins">
                                    MFIT COIN
                                </h5>
                            </div>
                        </a>
                    </div>

                    <div class="coin-info text-center">
                        <a href="javascript:void(0)">
                            <img src="images/apollon.svg" alt="">
                            <div class="mb-2">
                                <h5 class="font-bold no-margins">
                                    Apollon
                                </h5>
                            </div>
                        </a>
                    </div>

                    <div class="coin-info text-center">
                        <a href="javascript:void(0)">
                            <img src="images/bettex.svg" alt="">
                            <div class="mb-2">
                                <h5 class="font-bold no-margins">
                                    Bettex
                                </h5>
                            </div>
                        </a>
                    </div>

                    <div class="coin-info text-center">
                        <a href="javascript:void(0)">
                            <div class="ribbon ribbon-top-left"><span>New &amp; Hot</span></div>
                            <img src="images/mfit.svg" alt="">
                            <div class="mb-2">
                                <h5 class="font-bold no-margins">
                                    MFIT COIN
                                </h5>
                            </div>
                        </a>
                    </div>

                    <div class="coin-info text-center">
                        <a href="javascript:void(0)">
                            <img src="images/apollon.svg" alt="">
                            <div class="mb-2">
                                <h5 class="font-bold no-margins">
                                    Apollon
                                </h5>
                            </div>
                        </a>
                    </div>

                    <div class="coin-info text-center">
                        <a href="javascript:void(0)">
                            <img src="images/bettex.svg" alt="">
                            <div class="mb-2">
                                <h5 class="font-bold no-margins">
                                    Bettex
                                </h5>
                            </div>
                        </a>
                    </div>

                </div>
                <a href="#"><h4 style="float:right;">Suggest to your coin<b
                                style="padding-left: 10px;">+</b></h4></a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="partners">
                <h1 class="top-coins-section__title"> Partners </h1>

                <div class="coin-info text-center" style="margin-bottom: 34px">
                    <a href="javascript:void(0)">
                        <img src="images/kyd.svg" alt="">
                        <div class="mb-2">
                            <h5 class="font-bold no-margins">
                                KYD
                            </h5>
                        </div>
                    </a>
                </div>

                <a href="#"><h4 style="float:left;"><b style="padding-right: 10px;">+</b>Become a partner
                    </h4></a>
            </div>
        </div>
    </div>
</div>