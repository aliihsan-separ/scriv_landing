@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">
            @include('layouts.alerts')
            <div class="row">

                <div class="col-md-7">
                    <div class="section1">
                        <div class="box"><i class="fa fa-5x fa-check-square-o" aria-hidden="true"></i>
                            <span>No prepayment
                    <span>0% fee deposit and withdrawal</span></span>
                        </div>
                        <div class="box"><i class="fa fa-5x fa-check-square-o" aria-hidden="true"></i>
                            <span>Superior service security
                    <span>Veriﬁed by professionals - <a href="{!! url('/service_security') !!}">Learn More</a> </span></span>
                        </div>
                        <div class="box"><i class="fa fa-5x fa-check-square-o" aria-hidden="true"></i>
                            <span>Instant in everything
                    <span>Deposits, rewards, reinvest, withdrawal got no lock or delay.</span></span>
                        </div>

                    </div>
                    <div class="live"></div>
                </div>

                <div class="col-md-5"><h1></h1>
                    <div class="login">
                        <div class="title">Start using in a few clicks!</div>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link " id="home-tab" data-toggle="tab" href="#login" role="tab"
                                   aria-controls="home" aria-selected="true">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#signup" role="tab"
                                   aria-controls="profile" aria-selected="false">Sign Up</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade " id="login" role="tabpanel" aria-labelledby="home-tab">
                                <form class="signup" method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group ">
                                        <label for="e-mail">Email</label>
                                        <input type="email" name="email" class="form-control" id="e-mail"
                                               aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                    </div>

                                    <button type="submit" class="btn logbtn">Log In</button>

                                </form>
                            </div>
                            <div class="tab-pane fade show active" id="signup" role="tabpanel" aria-labelledby="profile-tab">
                                <form class="signup" method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form-group ">
                                        <label for="name">Full Name</label>
                                        <input type="text" name="name" class="form-control" id="name"
                                               aria-describedby="emailHelp" placeholder="Enter Full Name">
                                    </div>
                                    <div class="form-group ">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" class="form-control" id="email"
                                               aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="pass">Password</label>
                                        <input type="password" name="password" class="form-control" id="pass"
                                               placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="pass-confirm">Password confirmation</label>
                                        <input type="password" name="password_confirmation" class="form-control" id="pass-confirm"
                                               placeholder="Password">
                                    </div>

                                    <button type="submit" class="btn logbtn">Sign Up Now!</button>
                                </form>
                                <div class="text">By continuing, you agree with the Stack-of-Stake fees, terms, service
                                    agreement, privacy policy and cookie policy
                                </div>
                                <span class="or">or</span>
                                <span id="signtitle">Sign up with:</span>
                                <div class="socialup">
                                    <a href="javascript:void(0)"><img src="images/facebook.png" alt="facebook"></a>
                                    <a href="javascript:void(0)"><img src="images/google.png" alt="google+"></a>
                                    <a href="javascript:void(0)"><img src="images/twittericon.png" alt="twitter"></a>
                                    <a href="javascript:void(0)"><img src="images/discord.png" alt="discord"></a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="section2">
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="top-coins-section__title">Listed Coins</h1>
                        </div>
                        <div class="col-md-4">
                            <h1 class="top-coins-section__title">Official Partners</h1>
                        </div>
                        <div class="col-md-10">
                            <div class="listed">

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="coin-info text-center">
                                            <a href="javascript:void(0)">
                                                <div class="ribbon ribbon-top-left"><span>New &amp; Hot</span></div>
                                                <img src="images/mfit.svg" alt="">
                                                <div class="mb-2">
                                                    <h4 class="font-bold no-margins">
                                                        MFIT COIN
                                                    </h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="coin-info text-center">
                                            <a href="javascript:void(0)">
                                                <img src="images/apollon.svg" alt="">
                                                <div class="mb-2">
                                                    <h4 class="font-bold no-margins">
                                                        Apollon
                                                    </h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="coin-info text-center">
                                            <a href="javascript:void(0)">
                                                <img src="images/bettex.svg" alt="">
                                                <div class="mb-2">
                                                    <h4 class="font-bold no-margins">
                                                        Bettex
                                                    </h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="coin-info text-center">
                                            <a href="javascript:void(0)">
                                                <div class="ribbon ribbon-top-left"><span>New &amp; Hot</span></div>
                                                <img src="images/mfit.svg" alt="">
                                                <div class="mb-2">
                                                    <h4 class="font-bold no-margins">
                                                        MFIT COIN
                                                    </h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="coin-info text-center">
                                            <a href="javascript:void(0)">
                                                <img src="images/apollon.svg" alt="">
                                                <div class="mb-2">
                                                    <h4 class="font-bold no-margins">
                                                        Apollon
                                                    </h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="coin-info text-center">
                                            <a href="javascript:void(0)">
                                                <img src="images/bettex.svg" alt="">
                                                <div class="mb-2">
                                                    <h4 class="font-bold no-margins">
                                                        Bettex
                                                    </h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="partners">
                                <div class="col-12">
                                    <div class="coin-info text-center">
                                        <a href="javascript:void(0)">
                                            <img src="images/kyd.svg" alt="">
                                            <div class="mb-2">
                                                <h4 class="font-bold no-margins">
                                                    KYD
                                                </h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
