<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Scriv Landing Page</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"/>
    <link rel="stylesheet" href="{!! asset('/css/bootstrap.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/css/fancybox.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/css/font-awesome.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/css/style.css') !!}"/>



    {{--<link rel="canonical" href="https://fonts.adobe.com/fonts/museo-sans">--}}
    <script src='https://www.google.com/recaptcha/api.js?hl=en'></script>


</head>
<body>
<script>
    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            testAPI();
        } else {
            // The person is not logged into your app or we are unable to tell.
            document.getElementById('status').innerHTML = 'Please log ' +
                'into this app.';
        }
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: '341561133090713',
            cookie: true,  // enable cookies to allow the server to access
                           // the session
            xfbml: true,  // parse social plugins on this page
            version: 'v2.8' // use graph api version 2.8
        });

        // Now that we've initialized the JavaScript SDK, we call
        // FB.getLoginStatus().  This function gets the state of the
        // person visiting this page and can return one of three states to
        // the callback you provide.  They can be:
        //
        // 1. Logged into your app ('connected')
        // 2. Logged into Facebook, but not your app ('not_authorized')
        // 3. Not logged into Facebook and can't tell if they are logged into
        //    your app or not.
        //
        // These three cases are handled in the callback function.

        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });

    };

    // Load the SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function (response) {
            console.log('Successful login for: ' + response.name);
            document.getElementById('status').innerHTML =
                'Thanks for logging in, ' + response.name + '!';
        });
    }
</script>
<header>
    <a data-fancybox class="various fancybox fancybox.iframe"
       href="https://www.youtube.com/watch?v=z0e_MpAiX2U?controls=0">
        <div class="top-bg" style="background-image: url('{!! asset('images/video-thumb.svg') !!}')">

            <div class="content">
                <span> The ultimate masternode experience</span>
                <span><i class="fa fa-play-circle fa-2x" aria-hidden="true"></i></span>
                <span>watch a 2 min demo </span> <span>to see how it’s easy to use StackOfStake</span>
            </div>

        </div>
    </a>

</header>

<div class="clear-fix"></div>

<div class="main">
    <div class="container">
        @include('layouts.alerts')
        <div class="row">

            <div class="col-md-7">
                <div class="section1">
                    <div class="box"><i class="fa fa-5x fa-money" aria-hidden="true"></i>
                        <span>No prepayment
                    <span>0% fee deposit and withdrawal  </span></span>
                    </div>
                    <div class="box"><i class="fa fa-5x fa-server" aria-hidden="true"></i>
                        <span>Superior service security
                    <span>Veriﬁed by professionals - <a
                            href="{!! url('/service_security') !!}">Learn More</a> </span></span>
                    </div>
                    <div class="box"><i class="fa fa-5x fa-check-square-o" aria-hidden="true"></i>
                        <span>Instant in everything
                    <span>Deposits, rewards, reinvest, withdrawal got no lock or delay.  </span></span>
                    </div>

                </div>
                <div class="live mt-5">
                    @php($activities = \Spatie\Activitylog\Models\Activity::where('subject_id',null)->orderBy('created_at','desc')->take(10)->get())
                    @foreach($activities as $activity)
                        <div class="info-popup animated fadeInLeft slow" onclick="swapDiv(event,this);">
                            <small class="time">{!! $activity->created_at->format('H:i:s') !!}</small>
                            <div class="info">
                                {!! $activity->description !!} {{--<span style="font-size: 40px;"> 311 SCRIV</span>--}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-md-5"><h1></h1>
                <div class="login">
                    <div class="title">Start using in a few clicks!</div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link " id="home-tab" data-toggle="tab" href="#login" role="tab"
                               aria-controls="home" aria-selected="true">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#signup" role="tab"
                               aria-controls="profile" aria-selected="false">Sign Up</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade " id="login" role="tabpanel" aria-labelledby="home-tab">
                            <form class="signup" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group ">
                                    <label for="e-mail">Email</label>
                                    <input type="email" name="email" class="form-control" id="e-mail"
                                           aria-describedby="emailHelp" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" class="form-control" id="password"
                                           placeholder="Password">
                                </div>
                                <br>
                                <div align="center" class="g-recaptcha"
                                     data-sitekey="6LebcX0UAAAAAO9hvUjYF4a06LiE9MX7LYHAvLnr"></div>
                                <br>
                                <div class="form-group row">
                                    <div class="col-md-6 ">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember"
                                                   id="remember"
                                                   {{ old('remember') ? 'checked' : '' }} style="width: 30px;height: 30px; margin-top: 0px;">

                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <button type="submit" class="btn logbtn">Log In</button>

                            </form>

                            <div class="socialup">


                                <a href="javascript:void(0)"><img src="images/facebook.png" alt="facebook"></a>
                                <a href="javascript:void(0)"><img src="images/google.png" alt="google+"></a>
                                <a href="javascript:void(0)"><img src="images/twittericon.png" alt="twitter"></a>
                                <a href="javascript:void(0)"><img src="images/discord.png" alt="discord"></a>
                            </div>
                        </div>
                        <div class="tab-pane fade show active" id="signup" role="tabpanel"
                             aria-labelledby="profile-tab">
                            <form class="signup" method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form-group ">
                                    <label for="name">Full Name</label>
                                    <input type="text" name="name" class="form-control" id="name"
                                           aria-describedby="emailHelp" placeholder="Enter Full Name">
                                </div>
                                <div class="form-group ">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control" id="email"
                                           aria-describedby="emailHelp" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="pass">Password</label>
                                    <input type="password" name="password" class="form-control" id="pass"
                                           placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label for="pass-confirm">Password confirmation</label>
                                    <input type="password" name="password_confirmation" class="form-control"
                                           id="pass-confirm"
                                           placeholder="Password">
                                </div>

                                <button type="submit" class="btn logbtn">Sign Up Now!</button>
                            </form>
                            <div class="text">By continuing, you agree with the Stack-of-Stake fees, terms, service
                                agreement, privacy policy and cookie policy
                            </div>
                            <span class="or">or</span>
                            <span id="signtitle">Sign up with:</span>
                            <div class="socialup">
                                <a href="javascript:void(0)"><img src="images/facebook.png" alt="facebook"></a>
                                <a href="javascript:void(0)"><img src="images/google.png" alt="google+"></a>
                                <a href="javascript:void(0)"><img src="images/twittericon.png" alt="twitter"></a>
                                <a href="javascript:void(0)"><img src="images/discord.png" alt="discord"></a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="section2">
                <div class="row">
                    <div class="col-md-8">

                    </div>
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-9">
                        <div class="listed">
                            <h1 class="top-coins-section__title"> Listed Coins </h1>
                            <div class="row">

                                <div class="coin-info text-center">
                                    <a href="javascript:void(0)">
                                        <div class="ribbon ribbon-top-left"><span>New &amp; Hot</span></div>
                                        <img src="images/mfit.svg" alt="">
                                        <div class="mb-2">
                                            <h5 class="font-bold no-margins">
                                                MFIT COIN
                                            </h5>
                                        </div>
                                    </a>
                                </div>

                                <div class="coin-info text-center">
                                    <a href="javascript:void(0)">
                                        <img src="images/apollon.svg" alt="">
                                        <div class="mb-2">
                                            <h5 class="font-bold no-margins">
                                                Apollon
                                            </h5>
                                        </div>
                                    </a>
                                </div>

                                <div class="coin-info text-center">
                                    <a href="javascript:void(0)">
                                        <img src="images/bettex.svg" alt="">
                                        <div class="mb-2">
                                            <h5 class="font-bold no-margins">
                                                Bettex
                                            </h5>
                                        </div>
                                    </a>
                                </div>

                                <div class="coin-info text-center">
                                    <a href="javascript:void(0)">
                                        <div class="ribbon ribbon-top-left"><span>New &amp; Hot</span></div>
                                        <img src="images/mfit.svg" alt="">
                                        <div class="mb-2">
                                            <h5 class="font-bold no-margins">
                                                MFIT COIN
                                            </h5>
                                        </div>
                                    </a>
                                </div>

                                <div class="coin-info text-center">
                                    <a href="javascript:void(0)">
                                        <img src="images/apollon.svg" alt="">
                                        <div class="mb-2">
                                            <h5 class="font-bold no-margins">
                                                Apollon
                                            </h5>
                                        </div>
                                    </a>
                                </div>

                                <div class="coin-info text-center">
                                    <a href="javascript:void(0)">
                                        <img src="images/bettex.svg" alt="">
                                        <div class="mb-2">
                                            <h5 class="font-bold no-margins">
                                                Bettex
                                            </h5>
                                        </div>
                                    </a>
                                </div>

                            </div>
                            <a href="#"><h4 style="float:right;">Suggest to your coin<b
                                        style="padding-left: 10px;">+</b></h4></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="partners">
                            <h1 class="top-coins-section__title"> Partners </h1>

                            <div class="coin-info text-center" style="margin-bottom: 34px">
                                <a href="javascript:void(0)">
                                    <img src="images/kyd.svg" alt="">
                                    <div class="mb-2">
                                        <h5 class="font-bold no-margins">
                                            KYD
                                        </h5>
                                    </div>
                                </a>
                            </div>

                            <a href="#"><h4 style="float:left;"><b style="padding-right: 10px;">+</b>Become a partner
                                </h4></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-12">
            <div class="ph6 pt6 pt-0">
                <h2 class="text-center">Do you want to learn how successful we are?</h2>
                <h1 class="text-center mt4 mb-5">Dive into <a class="btn btn-primary btn-large" href="/Home/Info">Service
                        stats</a></h1>
            </div>
        </div>

    </div>
</div>
<footer class="footer-primary">
    <div class="container">
        <div class="row text-sm-left">
            <div class="col-md-3 pull-left">
                <a href="/" class="f-logo">
                    <img class="logo__image" src="https://stackofstake.com/images/logo.svg" alt="">
                    <span class="logo__text">StackOfStake (beta)</span>
                </a>
            </div>
            <div class="col-md-2">
                <ul class="footer-social x-center">
                    <li>
                        <a target="_blank" href="mailto:contact@stackofstake.com"><i class="fa fa-envelope"
                                                                                     aria-hidden="true"></i></a>
                    </li>
                    <li>
                    </li>
                    <li>
                        <a target="_blank" href="https://bitcointalk.org/index.php?topic=3295436"><i
                                class="fab fa-bitcoin" aria-hidden="true"></i></a>
                    </li>
                    <li>
                    </li>
                    <li>
                        <a target="_blank" href="https://discord.gg/tYcb8uK"><i class="fab fa-discord"
                                                                                aria-hidden="true"></i></a>
                    </li>
                    <li>
                    </li>
                    <li>
                        <a target="_blank" href="https://twitter.com/StackOfStake"><i class="fab fa-twitter"
                                                                                      aria-hidden="true"></i></a>
                    </li>
                    <li>
                    </li>
                </ul>
            </div>
            <div class="col-md-1 ">
                <ul class="list-unstyled">
                    <li><a href="https://stackofstake.com/Home/Partnership">Listing</a></li>
                    <li><a href="https://stackofstake.com/Home/Careers">Careers</a></li>
                    <li><a href="mailto:contact@stackofstake.com">Email us</a></li>
                </ul>
            </div>
            <div class="col-md-2 ">
                <ul class="list-unstyled">
                    <li><a href="https://stackofstake.com/Home/ServiceAgreement">Service Agreement</a></li>
                    <li><a href="https://stackofstake.com/Home/TermsOfUse">Terms Of Use</a></li>
                    <li><a href="https://stackofstake.com/Home/PrivacyPolicy">Privacy Policy</a></li>
                    <li><a href="https://stackofstake.com/Home/CookiePolicy">Cookie Policy</a></li>
                </ul>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-2"></div>

        </div>
    </div>
    <div class="container">
        <div class="col-sm-12">
            <p class="text-center footer-copyright">
                © 2018 - <a target="_blank" href="https://scriv.network">Scriv Network</a>, <a href="#">StackOfStake</a>.
                All Rights Reserved.
            </p>
        </div>
    </div>
    </div>
</footer>

<!--Jquery-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.js"></script>
<!--Bootstrap-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!-- Scipts -->
<script src="{!! asset('/js/main.js') !!}" type="text/javascript"></script>
<script type="text/javascript">

    function swapDiv(event, elem) {
        elem.parentNode.insertBefore(elem, elem.parentNode.firstChild);
    }

</script>
</body>
</html>
