<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }

    protected function validateLogin(Request $request)
    {
        $rules = [
            'email' => 'required|string',
            'password' => 'required|string',
            'g-recaptcha-response' => 'required|string',
        ];

        $this->validate($request,$rules);
        $request = $request->all();

        $control_humanity = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LebcX0UAAAAADIqqgpAuFjqNTho3uqPsHb3EZlN&response=" . $request['g-recaptcha-response'] . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        $control_humanity = json_decode($control_humanity);

        if($control_humanity->success == false){
            return redirect()->route('logout')->withErrors(['Whoopss.. Are we human?']);
        }
    }

    protected function authenticated(Request $request, $user)
    {
        activity()->log($user->name.', has logged in');
    }


}
