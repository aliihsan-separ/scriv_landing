<?php

namespace App\Http\Controllers;

use App\User;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function addPartnerLog(Request $request)
    {
        $user = User::find($request->user);
        $user_logs = Activity::where('causer_id',$user->id)->where('description','like','%partner%')->get();
        if(!count($user_logs)){
            $log = activity()->log($user->name.', is now our partner');
            return $log->description;
        }
        return 'error';
    }
}
